# MyTube website

HCL frontend training project.

## Installation

### `yarn install`

To install all neccesary packages.

## Development

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
