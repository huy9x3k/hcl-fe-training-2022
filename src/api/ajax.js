import axios from "axios";

export default axios.create({
  baseUrl:
    process.env.NODE_ENV === "development"
      ? "http://truly-contacts.herokuapp.com/api" // development API
      : "", // production API
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});
