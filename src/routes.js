import React from "react";
import { Route, Routes } from "react-router-dom";

import DetailLayout from "./layout/detail-layout/DetailLayout";
import MainLayout from "./layout/main-layout/MainLayout";
import HomePage from "./pages/home/HomePage";
import NotFoundPage from "./pages/not-found/NotFoundPage";

export default function Router() {
  return (
    <Routes>
      <Route path="*" element={<NotFoundPage />} />
      <Route element={<MainLayout />}>
        <Route path="/" element={<HomePage />} />
      </Route>
      <Route element={<DetailLayout />}>
        <Route path="/detail" element={<HomePage />} />
      </Route>
    </Routes>
  );
}
