import styles from './UserOptions.module.scss';

const UserOptions = () => {
  return (
    <div className={styles['user-options']}>
      <img src='/assets/images/video.png' className={styles.icon} alt='' />
      <img src='/assets/images/grid.png' className={styles.icon} alt='' />
      <img src='/assets/images/bell.png' className={styles.icon} alt='' />
      <div className={styles['user-dp']}>
        <img src='/assets/images/profile-pic.png' alt='' />
      </div>
    </div>
  );
};

export default UserOptions;
