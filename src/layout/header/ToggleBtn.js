import styles from './ToggleBtn.module.scss';
const ToggleBtn = ({ onClick, className }) => {
  return (
    <div
      className={`${styles['toggle-btn']} ${
        className !== undefined && className
      }`}
    >
      <div className={styles.burger} onClick={onClick}>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div className={styles.logo}>
        <img src='/assets/images/logo.png' alt='' />
      </div>
    </div>
  );
};

export default ToggleBtn;
