import styles from './SearchBox.module.scss';

const SearchBox = () => {
  return (
    <div className={styles['search-box']}>
      <input
        type='text'
        className={styles['search-bar']}
        placeholder='search'
      />
      <button className={styles['search-btn']}>
        <img src='/assets/images/search.png' alt='' />
      </button>
    </div>
  );
};

export default SearchBox;
