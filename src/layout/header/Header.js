import React from 'react';
import ToggleBtn from './ToggleBtn';
import SearchBox from './SearchBox';
import UserOptions from './UserOptions';
import styles from './Header.module.scss';

const Header = ({ onShowSidebar }) => {
  return (
    <nav className={styles.navbar}>
      <ToggleBtn onClick={onShowSidebar} />
      <SearchBox />
      <UserOptions />
    </nav>
  );
};

export default Header;
