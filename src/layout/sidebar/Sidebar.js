import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import ToggleBtn from '../header/ToggleBtn';
import styles from './Sidebar.module.scss';

const Sidebar = ({ onHideSidebar, sidebarDisplay }) => {
  return (
    <>
      {ReactDOM.createPortal(
        <div
          className={`${styles.backdrop} ${
            sidebarDisplay ? styles.visible : ''
          }`}
        ></div>,
        document.getElementById('overlay')
      )}
      <nav
        className={`${styles['side-bar']} ${
          sidebarDisplay ? styles.opened : ''
        }`}
      >
        <ToggleBtn onClick={onHideSidebar} className={styles['toggle-btn']} />
        <Link to='#' className={`${styles.links} ${styles.active}`}>
          <img src='/assets/images/home.png' alt='' />
          home
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/explore.png' alt='' />
          explore
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/subscription.png' alt='' />
          subscription
        </Link>
        <hr className={styles.seperator} />
        <Link to='#' className={styles.links}>
          <img src='/assets/images/library.png' alt='' />
          library
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/history.png' alt='' />
          history
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/your-video.png' alt='' />
          your video
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/watch-later.png' alt='' />
          watch leater
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/liked-video.png' alt='' />
          like video
        </Link>
        <Link to='#' className={styles.links}>
          <img src='/assets/images/show-more.png' alt='' />
          show more
        </Link>
      </nav>
    </>
  );
};

export default Sidebar;
