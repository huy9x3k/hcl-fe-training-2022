import React, { useState } from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../header/Header';
import Sidebar from '../sidebar/Sidebar';
import styles from './MainLayout.module.scss';

const MainLayout = () => {
  const [sidebarDisplay, setSidebarDisplay] = useState(false);

  const showSidebarHandler = () => {
    setSidebarDisplay(true);
  };
  const hideSidebarHandler = () => {
    setSidebarDisplay(false);
  };

  return (
    <div
      className={`${styles['main-layout']} ${
        sidebarDisplay ? styles['side-bar-visible'] : ''
      }`}
    >
      <Header onShowSidebar={showSidebarHandler} />
      <Sidebar
        onHideSidebar={hideSidebarHandler}
        sidebarDisplay={sidebarDisplay}
      />
      <Outlet className={styles.outlet} />
    </div>
  );
};

export default MainLayout;
