import React, { useCallback } from "react";
import { getUserTest } from "../../api/user";

const UserPage = () => {
  const fetchUser = useCallback(async () => {
    const response = await getUserTest();
    console.log(response);
  }, []);

  useEffect(() => {
    fetchUser();
  }, [fetchUser]);

  return <div></div>;
};

export default UserPage;
