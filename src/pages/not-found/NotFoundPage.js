import React from "react";
import styles from "./NotFoundPage.module.scss";

const NotFoundPage = () => {
  return (
    <div className={styles.notFoundPage}>
      Sorry, the page you are looking is not available.
    </div>
  );
};

export default NotFoundPage;
