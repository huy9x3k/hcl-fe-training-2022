import React from "react";
import { Link } from "react-router-dom";
import Button from "../../../shared/button/Button";
import styles from "./Heading.module.scss";

const Heading = () => {
  const isHomePage = window.location.pathname === "/"; // for testing purposes

  return (
    <div className={styles.heading}>
      <h1>Welcome to MyTube.</h1>
      <p>Enjoy the web</p>
      <Link to={isHomePage ? "/detail" : "/"}>
        <Button text={isHomePage ? "Go to detail" : "Go to home"} />
      </Link>
    </div>
  );
};

export default Heading;
