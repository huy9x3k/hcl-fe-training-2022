import React from "react";
import Heading from "./components/Heading";
import styles from "./HomePage.module.scss";

const HomePage = () => {
  return (
    <div className={styles.homePage}>
      {/* Welcome Heading */}
      <Heading />
    </div>
  );
};

export default HomePage;
